import UIKit
import UnityFramework

public final class UnityBridge: NSObject {
  public static let `default` = UnityBridge()

  /// UnityFramework 를 감싸는 Umbrella 프레임워크의 이름
  private let umbrellaFrameworkName = "Umbrella"

  /// `UnityFramework` 인스턴스
  private var unity: UnityFramework!

  /// Unity 윈도우가 생성되지 않았거나 숨겨졌을때 나타날 Window
  private var hostWindow: UIWindow!

  /// `UnityFramework` 초기화 여부 체크
  private var isUnityInitialized: Bool {
    unity != nil && unity.appController() != nil
  }

  override private init() { }
}

// MARK: - public methods

extension UnityBridge {

  /// Host 윈도우 지정. 이게 제일 먼저 호출되어야 한다.
  public func setHostWindow(_ window: UIWindow) {
    hostWindow = window
  }

  /// Unity 윈도우를 화면에 띄운다. 초기화되어있지 않으면 초기화한다.
  public func show() throws {
    guard isUnityInitialized else {
      initUnityWindow()
      return
    }

    if hostWindow == nil { return }

    unity.showUnityWindow()
  }

  /// Unity 윈도우를 화면에서 숨긴다.
  public func hide() {
    guard let hostWindow = hostWindow else { return }

    // hostWindow 를 맨 앞으로 가져온다. -> Unity 윈도우는 숨겨진다.
    hostWindow.makeKeyAndVisible()
  }

  /// Unity 윈도우를 언로드한다.
  public func unload() {
    guard isUnityInitialized else { return }

    unity.unloadApplication()
  }

  /// Unity GameObject 에 메시지를 보낸다.
  public func sendMessage(withName name: String, functionName: String, message: String) {
    guard isUnityInitialized else { return }

    unity.sendMessageToGO(
      withName: name,
      functionName: functionName,
      message: message
    )
  }

  /// Unity 윈도우를 종료한다.
  public func quit() {
    guard isUnityInitialized else { return }
    unity.quitApplication(0)
  }

  /// Unity 윈도우의 뷰를 얻는다.
  public func getView() -> UIView {
    unity.appController().rootView
  }
}

// MARK: - private methods

extension UnityBridge {

  /// Unity 윈도우를 초기화한다.
  private func initUnityWindow() {
    // UnityFramework 프레임워크를 로드한다.
    unity = loadUnityFramework()

    // `Data` 디렉토리가 들어있는 번들의 id.
    // UnityFramework 프레임워크 안에 넣었으므로 UnityFramework 프레임워크의 번들 id 를 지정한다.
    unity.setDataBundleId("com.unity3d.framework")

    // UnityFramework 관련 이벤트 리스너를 지정한다.
    unity.register(self)

    // Unity 윈도우를 초기화하고 띄운다.
    unity.runEmbedded(
      withArgc: CommandLine.argc,
      argv: CommandLine.unsafeArgv,
      appLaunchOpts: nil
    )
  }

  /// `UnityFramework` 프레임워크를 로드한다.
  private func loadUnityFramework() -> UnityFramework {
    // 앱 번들 내의 UnityFramework 프레임워크 경로
    let bundlePath = Bundle.main.bundlePath
      .appending("/Frameworks/\(umbrellaFrameworkName).framework/Frameworks/UnityFramework.framework")

    guard let bundle = Bundle(path: bundlePath) else {
      fatalError("UnityFramework 찾을 수 없음")
    }

    // 프레임워크가 로드되지 않았으면 로드
    if !bundle.isLoaded { bundle.load() }

    guard let ufw = bundle.principalClass?.getInstance() else {
      fatalError("UnityFramework load 실패")
    }

    return ufw
  }
}

// MARK: - UnityFrameworkListener

extension UnityBridge: UnityFrameworkListener {

  /// Unity 윈도우가 언로드 되면 호출된다.
  public func unityDidUnload(_ notification: Notification!) {
    // `UnityFramework.register(_:)` 의 반대
    unity.unregisterFrameworkListener(self)
  }

  /// Unity 윈도우 종료시 호출된다.
  public func unityDidQuit(_ notification: Notification!) {
    unity.unregisterFrameworkListener(self)
    unity = nil
  }
}
